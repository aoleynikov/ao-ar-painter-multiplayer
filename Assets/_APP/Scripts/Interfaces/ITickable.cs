﻿
namespace AO.ArPainter
{
    public interface ITickable
    {
        void Tick();
    }
}