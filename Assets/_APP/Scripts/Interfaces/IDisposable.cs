﻿
namespace AO.ArPainter
{
    public interface IDisposable
    {
        void Dispose();
    }
}