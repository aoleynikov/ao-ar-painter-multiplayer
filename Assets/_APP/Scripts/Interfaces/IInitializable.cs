﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AO.ArPainter
{
    public interface IInitializable
    {
        void Initialize(GameSettings gameSettings, AsyncProcessor asyncProcessor, Camera camera);
    }
}