﻿using System.Collections.Generic;
using UnityEngine;

namespace AO.ArPainter
{
	// Entry point of game
	public class GameBootstrap : MonoBehaviour
	{
		[SerializeField] private Camera _camera;

		private GameSettings _gameSettings;
		private AsyncProcessor _asyncProcessor;

		// all initializable systems in the game
		private readonly List<IInitializable> _initializables = new List<IInitializable>();
		// all tickable systems in the game
		private readonly List<ITickable> _tickables = new List<ITickable>();
		// all disposable systems in the game
		private readonly List<IDisposable> _disposables = new List<IDisposable>();

		private UISystem _uiSystem;
		private NetworkSystem _networkSystem;
		private DrawLinesSystem _drawLinesSystem;
		private TrackableEventSystem _trackableEventSystem;

		private void Start()
		{
			_gameSettings = Resources.Load("GameSettings") as GameSettings;

			if (!_gameSettings)
				return;


			_asyncProcessor = Instantiate(_gameSettings.asyncProcessor);

			_uiSystem = new UISystem();
			_initializables.Add(_uiSystem);
			_disposables.Add(_uiSystem);

			_networkSystem = new NetworkSystem();
			_initializables.Add(_networkSystem);
			_tickables.Add(_networkSystem);
			_disposables.Add(_networkSystem);

			_drawLinesSystem = new DrawLinesSystem();
			_initializables.Add(_drawLinesSystem);
			_tickables.Add(_drawLinesSystem);
			_disposables.Add(_drawLinesSystem);

			_trackableEventSystem = new TrackableEventSystem();
			_initializables.Add(_trackableEventSystem);
			_disposables.Add(_trackableEventSystem);


			foreach (var initializable in _initializables)
			{
				initializable.Initialize(_gameSettings, _asyncProcessor, _camera);
			}
		}

		private void Update()
		{
			foreach (var tickable in _tickables)
			{
				tickable.Tick();
			}
		}

		private void OnDisable()
		{
			foreach (var disposable in _disposables)
			{
				disposable.Dispose();
			}
		}

	}
}

