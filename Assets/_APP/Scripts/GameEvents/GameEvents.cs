﻿using UnityEngine;
using UnityEngine.Events;

namespace AO.ArPainter
{
    public class GameEvents
    {
        // called when start host button pressed
        public static UnityAction OnStartHost;
        // called when start client button pressed
        public static UnityAction<string> OnStartClient;

        // called when game started
        public static UnityAction OnGameStarted;
        // called when game finished
        public static UnityAction OnGameFinished;

        // called when trackable found
        public static UnityAction OnTrackableFound;
        // called when trackable lost
        public static UnityAction OnTrackableLost;

        // called when new local line started
        public static UnityAction<Color> OnLineStarted;
        // called when new local line updated
        public static UnityAction<Vector3> OnLineUpdated;
        // called when new local line finished
        public static UnityAction OnLineFinished;

        // called when new remote line started
        public static UnityAction<Color> OnRemoteLineStarted;
        // called when new remote line updated
        public static UnityAction<Vector3> OnRemoteLineUpdated;
        // called when new remote line finished
        public static UnityAction OnRemoteLineFinished;

        // show this device IP
        public static UnityAction<string> OnShowHostIP;
    }
}