﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AO.ArPainter
{
    [CreateAssetMenu(fileName = "GameSettings", menuName = "ArPainterMultiplayer/GameSettings", order = 1)]
    public class GameSettings : ScriptableObject
    {
        [Tooltip("Prefab of Async processor")]
        public AsyncProcessor asyncProcessor;
        [Tooltip("Prefab of game UI")]
        public GameUI gameUI;
        [Tooltip("Prefab of line")]
        public Line line;
    }
}