﻿using UnityEngine;
using UnityEngine.UI;

namespace AO.ArPainter
{
	public class GameUI : MonoBehaviour
	{
		[Tooltip("Main UI Animator")]
		public Animator uiAnimator;
		
		[Tooltip("Start Host Button")]
		public Button startHostButton;
		[Tooltip("This host IP")]
		public Text hostIP;

		[Tooltip("Start Client Button")]
		public Button startClientButton;
		[Tooltip("Host IP INput")]
		public InputField hostIpInput;

		[Tooltip("Reconnect Button")]
		public Button reconnectButton;
	}
}