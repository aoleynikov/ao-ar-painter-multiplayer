﻿
using System.Net;
using UnityEngine;

namespace AO.ArPainter
{
    public class UISystem : IInitializable, IDisposable
    {
        private GameUI _gameUI;
        private GameSettings _gameSettings;

        public void Initialize(GameSettings gameSettings, AsyncProcessor asyncProcessor, Camera camera)
        {
            _gameSettings = gameSettings;

            _gameUI = GameObject.Instantiate(_gameSettings.gameUI);

            // события по кнопкам
            _gameUI.startHostButton.onClick.AddListener(OnStartHostPressed);
            _gameUI.startClientButton.onClick.AddListener(OnStartClientPressed);
            _gameUI.reconnectButton.onClick.AddListener(OnReconnectPressed);

            GameEvents.OnShowHostIP += OnShowHostIP;

        }

        public void Dispose()
        {
            GameEvents.OnShowHostIP -= OnShowHostIP;
        }

        private void OnStartHostPressed()
        {
            _gameUI.uiAnimator.SetTrigger("Gameplay");
            GameEvents.OnStartHost.Invoke();
            GameEvents.OnGameStarted.Invoke();
        }

        private void OnStartClientPressed()
        {
            IPAddress ipAddress;

            if (IPAddress.TryParse(_gameUI.hostIpInput.text, out ipAddress))
            {
                _gameUI.uiAnimator.SetBool("Error", false);
                _gameUI.uiAnimator.SetTrigger("Gameplay");
                GameEvents.OnStartClient.Invoke(_gameUI.hostIpInput.text);
                GameEvents.OnGameStarted.Invoke();
            }
            else
            {
                _gameUI.uiAnimator.SetBool("Error", true);
            }
        }

        private void OnReconnectPressed()
        {
            _gameUI.uiAnimator.SetTrigger("Connection");
            GameEvents.OnGameFinished.Invoke();
        }

        private void OnShowHostIP(string ip)
        {
            _gameUI.hostIP.text = ip;
        }
    }
}