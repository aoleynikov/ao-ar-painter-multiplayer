﻿
using UnityEngine;
using Vuforia;

namespace AO.ArPainter
{
    public class TrackableEventSystem : IInitializable, IDisposable, ITrackableEventHandler
    {
        private TrackableBehaviour _trackableBehaviour;

        public void Initialize(GameSettings gameSettings, AsyncProcessor asyncProcessor, Camera camera)
        {
            _trackableBehaviour = Resources.FindObjectsOfTypeAll<TrackableBehaviour>()[0];

            if (_trackableBehaviour)
                _trackableBehaviour.RegisterTrackableEventHandler(this);
        }

        public void Dispose()
        {
            if (_trackableBehaviour)
                _trackableBehaviour.UnregisterTrackableEventHandler(this);
        }

        public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus,
            TrackableBehaviour.Status newStatus)
        {
            if (newStatus == TrackableBehaviour.Status.DETECTED ||
                newStatus == TrackableBehaviour.Status.TRACKED ||
                newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
            {
                OnTrackingFound();
            }
            else if (previousStatus == TrackableBehaviour.Status.TRACKED &&
                     newStatus == TrackableBehaviour.Status.NO_POSE)
            {
                OnTrackingLost();
            }
            else
            {
                OnTrackingLost();
            }
        }

        private void OnTrackingFound()
        {
            var rendererComponents = _trackableBehaviour.GetComponentsInChildren<Renderer>(true);
            var colliderComponents = _trackableBehaviour.GetComponentsInChildren<Collider>(true);
            var canvasComponents = _trackableBehaviour.GetComponentsInChildren<Canvas>(true);

            // Enable rendering:
            foreach (var component in rendererComponents)
                component.enabled = true;

            // Enable colliders:
            foreach (var component in colliderComponents)
                component.enabled = true;

            // Enable canvas':
            foreach (var component in canvasComponents)
                component.enabled = true;

            GameEvents.OnTrackableFound.Invoke();
        }


        private void OnTrackingLost()
        {
            var rendererComponents = _trackableBehaviour.GetComponentsInChildren<Renderer>(true);
            var colliderComponents = _trackableBehaviour.GetComponentsInChildren<Collider>(true);
            var canvasComponents = _trackableBehaviour.GetComponentsInChildren<Canvas>(true);

            // Disable rendering:
            foreach (var component in rendererComponents)
                component.enabled = false;

            // Disable colliders:
            foreach (var component in colliderComponents)
                component.enabled = false;

            // Disable canvas':
            foreach (var component in canvasComponents)
                component.enabled = false;

            GameEvents.OnTrackableLost.Invoke();
        }
    }
}