﻿
using Boo.Lang;
using UnityEngine;

namespace AO.ArPainter
{
    public class LineController : IInitializable, ITickable
    {
        private GameSettings _gameSettings;
        private Camera _camera;

        private List<Vector3> _linePoints = new List<Vector3>();

        private float _threshold = 0.001f;

        private int _lineCount = 0;

        private Vector3 _lastPos = Vector3.one * float.MaxValue;

        private bool _lineStarted = false;

        private Line _currentLine;

        private Transform _lineParent;

        public LineController(Transform lineParent)
        {
            _lineParent = lineParent;
        }

        public void Initialize(GameSettings gameSettings, AsyncProcessor asyncProcessor, Camera camera)
        {
            _gameSettings = gameSettings;
            _camera = camera;
        }

        public void StartNewLine(Color color)
        {
            _lineStarted = true;

            _linePoints.Clear();
            _currentLine = GameObject.Instantiate(_gameSettings.line);
            _currentLine.transform.parent = _lineParent;

            _currentLine.lineRenderer.startColor = color;
            _currentLine.lineRenderer.endColor = color;
        }

        public Vector3? UpdateLine()
        {
            Vector3 mousePos = Input.mousePosition;
            mousePos.z = 2 + Mathf.Sin(Time.time * 10);
            Vector3 mouseWorld = _camera.ScreenToWorldPoint(mousePos);

            float dist = Vector3.Distance(_lastPos, mouseWorld);
            
            if (dist <= _threshold)
                return null;

            _lastPos = mouseWorld;

            _linePoints.Add(mouseWorld);

            return mouseWorld;
        }

        public void AddPoint(Vector3 point)
        {
            _linePoints.Add(point);
        }

        public void FinishLine()
        {
            _lineStarted = false;
        }

        private void UpdateRenderer()
        {
            _currentLine.lineRenderer.SetVertexCount(_linePoints.Count);

            for (int i = _lineCount; i < _linePoints.Count; i++)
            {
                _currentLine.lineRenderer.SetPosition(i, _linePoints[i]);
            }
            _lineCount = _linePoints.Count;
        }

        public void Tick()
        {
            if (_lineStarted)
            {
                UpdateRenderer();
            }
        }

        public void DestroyLine()
        {
            GameObject.Destroy(_currentLine.gameObject);
        }
    }
}