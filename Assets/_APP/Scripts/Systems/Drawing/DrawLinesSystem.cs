﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace AO.ArPainter
{
    public class DrawLinesSystem : IInitializable, IDisposable, ITickable
    {
        private Transform _lineParent;

        private GameSettings _gameSettings;
        private Camera _camera;
        private AsyncProcessor _asyncProcessor;

        private List<LineController> _lines = new List<LineController>();
        private List<LineController> _deviceLines = new List<LineController>();

        private bool _gameStarted = false;
        private bool _trackableFound = false;

        private bool _lineStarted = false;


        public void Initialize(GameSettings gameSettings, AsyncProcessor asyncProcessor, Camera camera)
        {
            _gameSettings = gameSettings;
            _asyncProcessor = asyncProcessor;
            _camera = camera;

            LinesContainer[] linesContainers = Resources.FindObjectsOfTypeAll<LinesContainer>();
            if (linesContainers.Length > 0)
            {
                _lineParent = linesContainers[0].transform;
            }

            _lineParent.gameObject.SetActive(false);

            GameEvents.OnRemoteLineStarted += OnReceivedNewLineStarted;
            GameEvents.OnRemoteLineUpdated += OnReceviedLinePointAdded;
            GameEvents.OnRemoteLineFinished += OnReceivedNewLineFinished;

            GameEvents.OnGameStarted += OnGameStarted;
            GameEvents.OnGameFinished += OnGameFinished;

            GameEvents.OnTrackableFound += OnTrackableFound;
            GameEvents.OnTrackableLost += OnTrackableLost;
        }

        public void Dispose()
        {
            GameEvents.OnRemoteLineStarted -= OnReceivedNewLineStarted;
            GameEvents.OnRemoteLineUpdated -= OnReceviedLinePointAdded;
            GameEvents.OnRemoteLineFinished -= OnReceivedNewLineFinished;

            GameEvents.OnGameStarted -= OnGameStarted;
            GameEvents.OnGameFinished -= OnGameFinished;

            GameEvents.OnTrackableFound -= OnTrackableFound;
            GameEvents.OnTrackableLost -= OnTrackableLost;
        }

        public void Tick()
        {
            if (_gameStarted && !EventSystem.current.IsPointerOverGameObject()) //&& _trackableFound)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    LineController lineController = new LineController(_lineParent);
                    lineController.Initialize(_gameSettings, _asyncProcessor, _camera);

                    Color color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);

                    lineController.StartNewLine(color);

                    _lines.Add(lineController);

                    _lineStarted = true;

                    GameEvents.OnLineStarted.Invoke(color);
                }
                else if (Input.GetMouseButton(0))
                {
                    Vector3? position = _lines[_lines.Count - 1].UpdateLine();

                    if (position != null)
                    {
                        GameEvents.OnLineUpdated.Invoke(position.Value);
                    }
                }
                else if (Input.GetMouseButtonUp(0) && _lineStarted)
                {
                    _lines[_lines.Count - 1].FinishLine();

                    _lineStarted = false;

                    GameEvents.OnLineFinished.Invoke();
                }


                foreach (var line in _lines)
                {
                    line.Tick();
                }

                foreach (var line in _deviceLines)
                {
                    line.Tick();
                }
            }

        }

        private void OnReceivedNewLineStarted(Color color)
        {
            LineController lineController = new LineController(_lineParent);
            lineController.Initialize(_gameSettings, _asyncProcessor, _camera);
            lineController.StartNewLine(color);

            _deviceLines.Add(lineController);
        }


        private void OnReceviedLinePointAdded(Vector3 point)
        {
            _deviceLines[_deviceLines.Count - 1].AddPoint(point);
        }

        private void OnReceivedNewLineFinished()
        {
            _deviceLines[_deviceLines.Count - 1].FinishLine();
        }

        private void OnGameStarted()
        {
            _lineParent.gameObject.SetActive(true);
            _gameStarted = true;
        }

        private void OnGameFinished()
        {
            _lineParent.gameObject.SetActive(false);
            _gameStarted = false;

            // Clear all lines
            for (int i = _lines.Count - 1; i >= 0; i--)
            {
                _lines[i].DestroyLine();
                _lines.RemoveAt(i);
            }

            for (int i = _deviceLines.Count - 1; i >= 0; i--)
            {
                _deviceLines[i].DestroyLine();
                _deviceLines.RemoveAt(i);
            }
        }

        private void OnTrackableFound()
        {
            _lineParent.gameObject.SetActive(true);
            _trackableFound = true;
        }

        private void OnTrackableLost()
        {
            _lineParent.gameObject.SetActive(false);
            _trackableFound = false;
        }
    }
}