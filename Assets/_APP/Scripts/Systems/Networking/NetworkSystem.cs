﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

namespace AO.ArPainter
{
    public class NetworkSystem : IInitializable, IDisposable, ITickable
    {

        private HostController _hostController;
        private ClientController _clientController;

        private bool _hostStarted = false;
        private bool _clientStarted = false;

        public void Initialize(GameSettings gameSettings, AsyncProcessor asyncProcessor, Camera camera)
        {
            GameEvents.OnStartHost += StartHost;
            GameEvents.OnStartClient += StartClient;

            GameEvents.OnLineStarted += OnNewLineStarted;
            GameEvents.OnLineUpdated += OnPointAdded;
            GameEvents.OnLineFinished += OnNewLineFinished;

            GameEvents.OnGameFinished += CloseAllConnections;

            GameEvents.OnShowHostIP.Invoke(GetHostIP());
        }

        public void Dispose()
        {
            GameEvents.OnStartHost -= StartHost;
            GameEvents.OnStartClient -= StartClient;

            GameEvents.OnLineStarted -= OnNewLineStarted;
            GameEvents.OnLineUpdated -= OnPointAdded;
            GameEvents.OnLineFinished -= OnNewLineFinished;

            GameEvents.OnGameFinished -= CloseAllConnections;

            CloseAllConnections();

        }

        public void Tick()
        {
            if (_hostController != null && _hostStarted)
            {
                _hostController.Tick();
            }
        }

        private void StartHost()
        {
            CloseAllConnections();

            _hostController = new HostController();
            _hostController.StartServer(7777);

            _hostStarted = true;
        }

        private void StartClient(string hostIp)
        {
            CloseAllConnections();

            _clientController = new ClientController();
            _clientController.StartClient(hostIp, 7777);

            _clientStarted = true;
        }

        private void CloseAllConnections()
        {
            if (_clientController != null && _clientStarted)
            {
                _clientController.Dispose();
                _clientController = null;

                _clientStarted = false;
            }

            if (_hostController != null && _hostStarted)
            {
                _hostController.Dispose();
                _hostController = null;

                _hostStarted = false;
            }
        }


        private void OnNewLineStarted(Color color)
        {
            if (_hostStarted)
            {
                _hostController.SendNewLineStartedToClient(color);
            }
        }

        private void OnPointAdded(Vector3 point)
        {
            if (_hostStarted)
            {
                _hostController.SendLinePointToClient(point);
            }
        }

        private void OnNewLineFinished()
        {
            if (_hostStarted)
            {
                _hostController.SendNewLineFinishedToClient();
            }
        }

        private string GetHostIP()
        {
            IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());
            foreach (IPAddress addr in localIPs)
            {
                if (addr.AddressFamily == AddressFamily.InterNetwork)
                {
                    return addr.ToString();
                }
            }

            return "localhost";
        }
    }
}