﻿using UnityEngine;
using UnityEngine.Networking;

namespace AO.ArPainter
{
    public class ProtocolMsg
    {
        public const short TimeStamp = 100;
        public const short LineStart = 101;
        public const short LineUpdate = 102;
        public const short LineFinish = 103;
    }

    public class LineStartedMessage : MessageBase
    {
        public float timeStamp;
        public Color color;
    }

    public class PointAddedMessage : MessageBase
    {
        public Vector3 point;
    }

    public class LineFinishedMessage : MessageBase
    {
        public float timeStamp;
    }


}