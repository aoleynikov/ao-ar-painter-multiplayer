﻿
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

namespace AO.ArPainter
{
    public class ClientController : IDisposable
    {
        private NetworkClient networkClient;


        public ClientController()
        {
            GameEvents.OnLineStarted += SendNewLineStartedToServer;
            GameEvents.OnLineUpdated += SendLinePointToServer;
            GameEvents.OnLineFinished += SendNewLineFinishedToServer;

        }

        public void Dispose()
        {
            StopClient();

            GameEvents.OnLineStarted -= SendNewLineStartedToServer;
            GameEvents.OnLineUpdated -= SendLinePointToServer;
            GameEvents.OnLineFinished -= SendNewLineFinishedToServer;
        }

        public void StartClient(string host, int port)
        {
            StopClient();

            networkClient = new NetworkClient();
            RegisterHandler();
            networkClient.Connect(host, port);
        }

        private void StopClient()
        {
            if (networkClient != null)
            {
                UnregisterHandler();
                networkClient.Disconnect();
                networkClient.Shutdown();
                networkClient = null;
            }
        }

        void RegisterHandler()
        {
            networkClient.RegisterHandler(MsgType.Connect, OnConnect);
            networkClient.RegisterHandler(MsgType.Disconnect, OnDisconnect);

            networkClient.RegisterHandler(ProtocolMsg.LineStart, OnServerLineStart);
            networkClient.RegisterHandler(ProtocolMsg.LineUpdate, OnServerLineUpdate);
            networkClient.RegisterHandler(ProtocolMsg.LineFinish, OnServerLineFinish);

        }

        void UnregisterHandler()
        {
            networkClient.UnregisterHandler(MsgType.Connect);
            networkClient.UnregisterHandler(MsgType.Disconnect);
            networkClient.UnregisterHandler(ProtocolMsg.TimeStamp);
        }

        #region ServerStatus

        private void OnDisconnect(NetworkMessage netMsg)
        {
            Debug.Log("Server disconnect");
            StartClient(networkClient.serverIp, networkClient.serverPort);
        }

        private void OnConnect(NetworkMessage netMsg)
        {
            Debug.Log("Server connect");
        }

        #endregion

        #region ClientCommand

        private void SendNewLineStartedToServer(Color color)
        {
            var msg = new LineStartedMessage();
            msg.timeStamp = Time.time;
            msg.color = color;

            networkClient.Send(ProtocolMsg.LineStart, msg);
        }


        public void SendLinePointToServer(Vector3 point)
        {
            var msg = new PointAddedMessage();
            msg.point = point;

            networkClient.Send(ProtocolMsg.LineUpdate, msg);
        }

        public void SendNewLineFinishedToServer()
        {
            var msg = new LineFinishedMessage();
            msg.timeStamp = Time.time;

            networkClient.Send(ProtocolMsg.LineFinish, msg);
        }

        #endregion


        private void OnServerLineStart(NetworkMessage netMsg)
        {
            var msg = netMsg.ReadMessage<LineStartedMessage>();

            GameEvents.OnRemoteLineStarted.Invoke(msg.color);
        }

        private void OnServerLineUpdate(NetworkMessage netMsg)
        {
            var msg = netMsg.ReadMessage<PointAddedMessage>();

            GameEvents.OnRemoteLineUpdated.Invoke(msg.point);
        }

        private void OnServerLineFinish(NetworkMessage netMsg)
        {
            var msg = netMsg.ReadMessage<LineFinishedMessage>();

            GameEvents.OnRemoteLineFinished.Invoke();
        }

    }
}