﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace AO.ArPainter
{
    public class HostController : ITickable, IDisposable
    {
        private NetworkServerSimple networkServer;

        private Dictionary<string, NetworkConnection> _connections = new Dictionary<string, NetworkConnection>();


        public void Tick()
        {
            if (networkServer != null)
            {
                networkServer.Update();
            }
        }

        public void Dispose()
        {
            StopServer();
        }

        public void StartServer(int port)
        {
            StopServer();

            networkServer = new NetworkServerSimple();
            RegisterHandler();
            networkServer.Listen(port);
        }

        private void StopServer()
        {
            if (networkServer != null)
            {
                UnregisterHandler();
                networkServer.Stop();
                NetworkTransport.Shutdown();
                networkServer = null;
            }
        }

        void RegisterHandler()
        {
            networkServer.RegisterHandler(MsgType.Connect, OnConnect);
            networkServer.RegisterHandler(MsgType.Disconnect, OnDisconnect);

            networkServer.RegisterHandler(ProtocolMsg.LineStart, OnClientLineStart);
            networkServer.RegisterHandler(ProtocolMsg.LineUpdate, OnClientLineUpdate);
            networkServer.RegisterHandler(ProtocolMsg.LineFinish, OnClientLineFinish);
        }

        void UnregisterHandler()
        {
            networkServer.UnregisterHandler(MsgType.Connect);
            networkServer.UnregisterHandler(MsgType.Disconnect);

            networkServer.UnregisterHandler(ProtocolMsg.TimeStamp);
            networkServer.UnregisterHandler(ProtocolMsg.LineStart);
            networkServer.UnregisterHandler(ProtocolMsg.LineUpdate);
            networkServer.UnregisterHandler(ProtocolMsg.LineFinish);
        }

        #region ClientStatus

        private void OnDisconnect(NetworkMessage netMsg)
        {
            _connections.Remove(netMsg.conn.address);
            Debug.Log("Client disconnect " + _connections.Count);
        }

        private void OnConnect(NetworkMessage netMsg)
        {
            Debug.Log("Client connect");

            if (!_connections.ContainsKey(netMsg.conn.address))
            {
                _connections.Add(netMsg.conn.address, netMsg.conn);
            }
        }

        #endregion

        #region ClientCommand

        private void OnClientLineStart(NetworkMessage netMsg)
        {
            var msg = netMsg.ReadMessage<LineStartedMessage>();

            GameEvents.OnRemoteLineStarted.Invoke(msg.color);
        }

        private void OnClientLineUpdate(NetworkMessage netMsg)
        {
            var msg = netMsg.ReadMessage<PointAddedMessage>();

            GameEvents.OnRemoteLineUpdated.Invoke(msg.point);
        }

        private void OnClientLineFinish(NetworkMessage netMsg)
        {
            var msg = netMsg.ReadMessage<LineFinishedMessage>();

            GameEvents.OnRemoteLineFinished.Invoke();
        }

        #endregion

        #region send commands

        public void SendNewLineStartedToClient(Color color)
        {
            var msg = new LineStartedMessage();
            msg.timeStamp = Time.time;
            msg.color = color;

            foreach (var connection in _connections)
            {
                if (connection.Value != null)
                {
                    connection.Value.Send(ProtocolMsg.LineStart, msg);
                }
            }
        }

        public void SendLinePointToClient(Vector3 point)
        {
            var msg = new PointAddedMessage();
            msg.point = point;

            foreach (var connection in _connections)
            {
                if (connection.Value != null)
                {
                    connection.Value.Send(ProtocolMsg.LineUpdate, msg);
                }
            }
        }

        public void SendNewLineFinishedToClient()
        {
            var msg = new LineFinishedMessage();
            msg.timeStamp = Time.time;

            foreach (var connection in _connections)
            {
                if (connection.Value != null)
                {
                    connection.Value.Send(ProtocolMsg.LineFinish, msg);
                }
            }
        }

        #endregion


    }
}